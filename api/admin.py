from django.contrib import admin
from django.utils.translation import ugettext_lazy as _
from django.contrib.auth.admin import UserAdmin as BaseUserAdmin

from .models import *
from chat.models import Conversation, Message

@admin.register(Message)
class MessageAdmin(admin.ModelAdmin):
    model = Message
    list_display = (
        'conversation',
        'sender',
        'timestamp',
        'message',
    )

class MessageInline(admin.StackedInline):
    model = Message
    can_delete = False

@admin.register(Conversation)
class ConversationAdmin(admin.ModelAdmin):
    model = Conversation
    list_display = ('participant_1', 'participant_2')
    inlines = (
        MessageInline,)




class UserProfileInline(admin.StackedInline):
    model = UserProfile
    can_delete = False


class FacebookProfileInline(admin.TabularInline):
    model = FacebookProfile


class GoogleProfileInline(admin.TabularInline):
    model = GoogleProfile


class SellInline(admin.TabularInline):
    model = Item.sell.through


class BuyInline(admin.TabularInline):
    model = Item.buy.through


@admin.register(Item)
class ItemAdmin(admin.ModelAdmin):
    model = Item
    list_display = ('code', 'game', 'type')


@admin.register(Game)
class GameAdmin(admin.ModelAdmin):
    model = Game
    list_display = ('name', )


@admin.register(FacebookProfile)
class FacebookProfileAdmin(admin.ModelAdmin):
    model = FacebookProfile
    list_display = ('email', )

    def delete(self, obj):
        return format_html(
            '<a class="btn" href="/admin/api/facebookprofile/{}/delete/">Delete</a>',
            obj.pk)

    delete.allow_tags = True
    delete.short_description = 'Delete object'
    list_display = (
        'email',
        'delete',
    )


@admin.register(GoogleProfile)
class GoogleProfileAdmin(admin.ModelAdmin):
    model = GoogleProfile
    list_display = ('email', )

    def delete(self, obj):
        return format_html(
            '<a class="btn" href="/admin/api/googleprofile/{}/delete/">Delete</a>',
            obj.pk)

    delete.allow_tags = True
    delete.short_description = 'Delete object'
    list_display = (
        'email',
        'delete',
    )


@admin.register(User)
class UserAdmin(BaseUserAdmin):
    inlines = (
        UserProfileInline,
        FacebookProfileInline,
        GoogleProfileInline,
        SellInline,
        BuyInline,
    )
    fieldsets = (
        (None, {
            'fields': ('email', 'password')
        }),
        # (_('Personal info'), {'fields': ('first_name', 'last_name','username')}),
        # (_('Permissions'), {'fields': ('is_active', 'is_staff', 'is_superuser',
        #                                'groups', 'user_permissions')}),
        # (_('Important dates'), {'fields': ('last_login', 'date_joined')}),
        # (_('Preferences'),{'fields':('preferences__type','preferences__game')}),
    )
    add_fieldsets = ((None, {
        'classes': ('wide', ),
        'fields': ('email', 'password1', 'password2'),
    }), )

    def delete(self, obj):
        return format_html(
            '<a class="btn" href="/admin/api/user/{}/delete/">Delete</a>',
            obj.pk)

    def fb_profile(self, obj):
        return obj.facebook_profile.email

    def g_profile(self, obj):
        return obj.google_profile.email

    def buy_count(self, obj):
        return obj.buy.count()

    def sell_count(self, obj):
        return obj.sell.count()

    delete.allow_tags = True
    delete.short_description = 'Delete object'

    list_display = ('email', 'username', 'fb_profile', 'g_profile', 'delete',
                    'buy_count', 'sell_count')
    search_fields = ('email', 'first_name', 'last_name')
    ordering = ('email', )
