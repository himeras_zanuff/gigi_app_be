from config import settings
from api.models import GoogleProfile,User
from django.http import request
from rest_framework_simplejwt.tokens import RefreshToken
from rest_framework.decorators import api_view
from rest_framework.decorators import authentication_classes, permission_classes
from rest_framework.response import Response
import requests
import json
import jwt

from google.oauth2 import id_token
from google.auth.transport import requests as google_request
import uuid


def g_get_create_user(g_profile,g_data):
    try:
        user = User.objects.get(google_profile=g_profile)
    except User.DoesNotExist:
        try:
            user = User.objects.get(email=g_data['email'])
            user.google_profile = g_profile
            g_profile.user = user
            g_profile.save()
            user.save()
        except User.DoesNotExist:
            user = User.objects.create(email=g_data['email'],username= uuid.uuid1(),google_profile=g_profile)
            g_profile.user = user
            g_profile.save()
            user.save()
    return user


def g_get_data(g_token):
    URL = 'https://oauth2.googleapis.com/tokeninfo'
    HEADERS= {"Accept": "application/json"}
    PARAMS = {'id_token':g_token}
    gResponse = requests.get(url = URL, params = PARAMS,headers = HEADERS) 
    print(gResponse.json())
    return gResponse.json()

def g_get_create_profile(g_token,g_data):
    try:
        g_profile = GoogleProfile.objects.get(email = g_data['email'])
    except GoogleProfile.DoesNotExist:
        g_profile = GoogleProfile.objects.create(email=g_data['email'],token=g_token)
    return g_profile

def g_update_profile(g_profile,g_data):
    g_profile.name = g_data['name']
    g_profile.g_id = g_data['sub']
    g_profile.picture = g_data['picture']
    g_profile.save()

def get_tokens_for_user(user):
    refresh = RefreshToken.for_user(user)
    return {
        'refresh': str(refresh),
        'access': str(refresh.access_token),
    }
def g_validate_token(g_token):
    gResponse = id_token.verify_oauth2_token(g_token, google_request.Request(), settings.GOOGLE_CLIENT_ID)
    if gResponse['iss'] not in ['accounts.google.com', 'https://accounts.google.com']:
        raise ValueError('Wrong issuer.')
    userid = gResponse['sub']

@api_view(['POST'])
@authentication_classes([])
@permission_classes([])
def g_login(request):
    jsonPayload = json.loads(request.body.decode('utf-8'))
    g_token = jsonPayload['token']
    g_validate_token(g_token)
    g_data = g_get_data(g_token)
    g_profile = g_get_create_profile(g_token,g_data)
    g_update_profile(g_profile,g_data)
    user = g_get_create_user(g_profile,g_data)
    jwt_token = get_tokens_for_user(user)
    return Response({"JWT": jwt_token})

@api_view(['POST'])
def g_link(request):
    jsonPayload = json.loads(request.body.decode('utf-8'))
    g_token = jsonPayload['token']
    g_validate_token(g_token)
    auth_header = request.META.get('HTTP_AUTHORIZATION')
    decoded_jwt = jwt.decode(auth_header.split(' ')[1],'n&dc^@zl(ky18fz4ib==4ilyxe%iwa7717#7)&*816!h$u*(3k')
    user = User.objects.get(id=decoded_jwt['user_id'])
    g_data = g_get_data(g_token)
    g_profile = g_get_create_profile(g_token,g_data)
    g_update_profile(g_profile,g_data)
    g_profile.user = user
    g_profile.save()
    user.google_profile = g_profile
    user.save()
    jwt_token = get_tokens_for_user(user)
    return Response({"JWT": jwt_token})