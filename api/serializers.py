from rest_framework import serializers
from api.models import *
from djoser import permissions


class UserProfileSerializer(serializers.ModelSerializer):
    class Meta:
        model=UserProfile
        fields = ('full_name','dob', 'country', 'city','photo')
    photo = serializers.ImageField(
        max_length=None,
        use_url=True
    )

class FacebookProfileSerializer(serializers.ModelSerializer):
    class Meta:
        model=FacebookProfile
        fields= ('email','fb_id','name','picture')

class GoogleProfileSerializer(serializers.ModelSerializer):
    class Meta:
        model=GoogleProfile
        fields= ('email','name','picture')

class ItemSerializer(serializers.ModelSerializer):
    class Meta:
        model=Item
        fields= ('code','description','type','type_description')

class GameSerializer(serializers.ModelSerializer):
    item_field=ItemSerializer(read_only = True,many=True,)
    class Meta:
        model=Game
        fields= ('name','description','image','item_field','key')

class UserSerializer(serializers.ModelSerializer ):
    profile=UserProfileSerializer(read_only = True)
    facebook_profile=FacebookProfileSerializer(read_only = True)
    google_profile=GoogleProfileSerializer(read_only = True)
    sell=ItemSerializer(read_only=True,many=True,)
    buy=ItemSerializer(read_only=True,many=True,)
    class Meta:
        model = User
        fields = ('id','username','email', 'facebook_profile','google_profile','profile','sell','buy',)

    def get(self):
        return self.username
