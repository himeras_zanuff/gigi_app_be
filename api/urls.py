from rest_framework import routers
from api.views import *
from api.utils import *
from api.social_api import ActivateUser, check_email, facebook_validate, google_validate

from django.urls import path,include
from django.conf.urls import url
from django.conf import settings
from django.conf.urls.static import static
from django.contrib import admin
from api.facebook_api import fb_link,fb_login
from api.google_api import g_login,g_link

router = routers.DefaultRouter()
router.register(r'userData', UserViewSet, basename='User')
router.register(r'items', ItemViewSet, basename='Item')
router.register(r'games', GamesViewSet, basename='Game')

urlpatterns = [
    url(r'^', include(router.urls)),
    url(r'^google_login/',g_login),
    url(r'^google_link/',g_link),
    url(r'^facebook_login/',fb_login),
    url(r'^facebook_link/',fb_link),
    url(r'^store_google_data/',google_validate),
    url(r'^check_email/',check_email),
    url(r'^activate/(?P<uid>[\w-]+)/(?P<token>[\w-]+)/$', ActivateUser.as_view()),
    url(r'^auth/', include('djoser.urls')),
    url(r'^auth/', include('djoser.urls.jwt')),
    url(r'^profilePicture/',upload_profile_pic),
    url(r'^update_sell_items/',update_sell_items),
    url(r'^update_buy_items/',update_buy_items),
]
