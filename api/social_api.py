from rest_framework.decorators import api_view
from rest_framework.decorators import authentication_classes, permission_classes
from rest_framework.response import Response
import json
import requests
from api.models import *
import uuid
from config import settings
import jwt
from rest_framework_simplejwt.tokens import RefreshToken
from django.core.exceptions import ObjectDoesNotExist

from rest_framework.generics import GenericAPIView

from google.oauth2 import id_token
from google.auth.transport import requests as google_request

class ActivateUser(GenericAPIView):
    def get(self, request, uid, token, format = None):
        payload = {'uid': uid, 'token': token}
        url = request.scheme+"://"+request.META['HTTP_HOST']+"/api/auth/users/activation/"
        # url = "http://127.0.0.1:8000/api/auth/users/activation/"
        response = requests.post(url, data = payload)
        if response.status_code == 204:
            return Response({"Success"}, response.status_code)
        else:
            return Response(response.json())

@api_view(['GET'])
@authentication_classes([])
@permission_classes([])
def check_email(request):
    jsonPayload = json.loads(request.body.decode('utf-8'))
    email = jsonPayload['email']
    user = User.objects.get(email=email)
    try:
        user = User.objects.get(email=email)
        return Response({"exists": True})
    except User.DoesNotExist:
        return Response({"exists": False})

@api_view(['POST'])
@authentication_classes([])
@permission_classes([])
def facebook_validate(request):
    auth_header = request.META.get('HTTP_AUTHORIZATION')
    jsonPayload = json.loads(request.body.decode('utf-8'))
    token = jsonPayload['token']
    if auth_header!=None:
        decoded_jwt = jwt.decode(auth_header.split(' ')[1],'n&dc^@zl(ky18fz4ib==4ilyxe%iwa7717#7)&*816!h$u*(3k')
        old_user = User.objects.get(id=decoded_jwt['user_id'])
    else:
        URL = 'https://graph.facebook.com/v6.0/me'
        HEADERS= {"Accept": "application/json"}
        PARAMS= {'fields':'email','access_token':token}
        fbResponse = requests.get(url = URL, params = PARAMS,headers = HEADERS)
        try:
            old_user = User.objects.get(email = fbResponse.json()['email'])
        except User.DoesNotExist:
            old_user = None

    try:
        if old_user:
            old_fb = FacebookProfile.objects.get(email=old_user.email)
            old_fb.user = old_user
            old_fb.save()
            return Response({"JWT": get_tokens_for_user(old_user)})
        else:
            raise FacebookProfile.DoesNotExist
    except FacebookProfile.DoesNotExist:
        URL = 'https://graph.facebook.com/v6.0/me'
        HEADERS= {"Accept": "application/json"}
        PARAMS = {'fields':['email'],'access_token':token}
        fbResponse = requests.get(url = URL, params = PARAMS,headers = HEADERS)
        jwt_token = create_user(fbResponse.json()['email'],"fb",token,old_user)
        load_facebook_data(token)
        return Response({"JWT": jwt_token})


def load_facebook_data(token):
    URL = 'https://graph.facebook.com/v6.0/me'
    HEADERS= {"Accept": "application/json"}
    PARAMS= {'fields':'name,picture.width(800).height(800),email,id','access_token':token}
    fbResponse = requests.get(url = URL, params = PARAMS,headers = HEADERS)
    fb_profile = FacebookProfile.objects.get(email = fbResponse.json()['email'])
    fb_profile.name = fbResponse.json()['name']
    fb_profile.fb_id = fbResponse.json()['id']
    fb_profile.picture = fbResponse.json()['picture']['data']['url']
    fb_profile.save()

def load_google_data(token):
    URL = 'https://oauth2.googleapis.com/tokeninfo'
    HEADERS= {"Accept": "application/json"}
    PARAMS = {'id_token':token}
    gResponse = requests.get(url = URL, params = PARAMS,headers = HEADERS)
    g_profile = GoogleProfile.objects.get(email=gResponse.json()['email'])
    g_profile.name = gResponse.json()['name']
    g_profile.picture = gResponse.json()['picture']
    g_profile.save()


@api_view(['POST'])
@authentication_classes([])
@permission_classes([])
def google_validate(request):
    auth_header = request.META.get('HTTP_AUTHORIZATION')
    jsonPayload = json.loads(request.body.decode('utf-8'))
    token = jsonPayload['token']
    if auth_header!=None:
        decoded_jwt = jwt.decode(auth_header.split(' ')[1],'n&dc^@zl(ky18fz4ib==4ilyxe%iwa7717#7)&*816!h$u*(3k')
        old_user = User.objects.get(id=decoded_jwt['user_id'])
    else:
        try:
            gResponse = id_token.verify_oauth2_token(token, google_request.Request(), settings.GOOGLE_CLIENT_ID)
            if gResponse['iss'] not in ['accounts.google.com', 'https://accounts.google.com']:
                raise ValueError('Wrong issuer.')
            userid = gResponse['sub']
        except ValueError:
            pass

        try:
            old_user = User.objects.get(email = gResponse['email'])
        except User.DoesNotExist:
            old_user = None

    try:
        if old_user:
            old_g = GoogleProfile.objects.get(email=old_user.email)
            old_g.user = old_user
            old_g.save()
            return Response({"JWT": get_tokens_for_user(old_user)})
        else:
            raise GoogleProfile.DoesNotExist
    except GoogleProfile.DoesNotExist:
        try:
            gResponse = id_token.verify_oauth2_token(token, google_request.Request(), settings.GOOGLE_CLIENT_ID)
            if gResponse['iss'] not in ['accounts.google.com', 'https://accounts.google.com']:
                raise ValueError('Wrong issuer.')
            userid = gResponse['sub']
        except ValueError:
            pass
        jwt_token = create_user(gResponse['email'],"g",token,old_user)
        load_google_data(token)
        return Response({"JWT": jwt_token})


def create_user(email,type,token,user_object):
    try:
        if(user_object == None):
            user = User.objects.get(email=email)
        else:
            user = user_object
    except User.DoesNotExist:
        id = uuid.uuid4()
        user = User.objects.create_user(username=id,email=email)

    if type=='fb':
        try:
            if(user_object == None):
                profile = FacebookProfile.objects.get(email=email)
            else:
                profile = FacebookProfile.objects.get(user = user)
            profile.token = token
            profile.save()
            user.facebook_profile = profile
            user.save()
        except FacebookProfile.DoesNotExist:
            new_profile = FacebookProfile.objects.create(email=email,token=token,user = user)

    elif type=='g':

        try:
            profile = GoogleProfile.objects.get(email=email)
            profile.token = token
            profile.save()
            user.google_profile = profile
            user.save()
        except GoogleProfile.DoesNotExist:
            user.google_profile = GoogleProfile.objects.create(email=email,token=token,user = user)

    else:
        return Exception()
    return get_tokens_for_user(user)

def get_tokens_for_user(user):
    refresh = RefreshToken.for_user(user)

    return {
        'refresh': str(refresh),
        'access': str(refresh.access_token),
    }
