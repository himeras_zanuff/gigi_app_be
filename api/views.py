
from api.models import  *
from api.serializers import *
from djoser import permissions
from rest_framework import viewsets
import json
import jwt
from rest_framework.response import Response


class ItemViewSet(viewsets.ModelViewSet):
    permission_classes=[permissions.CurrentUserOrAdminOrReadOnly,]
    queryset = Item.objects.all()
    serializer_class = ItemSerializer

class GamesViewSet(viewsets.ModelViewSet):
    permission_classes=[permissions.CurrentUserOrAdminOrReadOnly,]
    queryset = Game.objects.all()
    serializer_class = GameSerializer

class UserViewSet(viewsets.ModelViewSet):
    permission_classes=[permissions.CurrentUserOrAdmin,]
    def get_queryset(self):
        return User.objects.filter(id=self.request.user.id)
    serializer_class = UserSerializer


    def put(self, request, *args, **kwargs):
        auth_header = request.META.get('HTTP_AUTHORIZATION')
        jsonPayload = json.loads(request.body.decode('utf-8'))

        decoded_jwt = jwt.decode(auth_header.split(' ')[1],'n&dc^@zl(ky18fz4ib==4ilyxe%iwa7717#7)&*816!h$u*(3k')
        current_user = User.objects.get(id=decoded_jwt['user_id'])
        print(current_user)
        # print(jsonPayload)
        for key in jsonPayload:
            value  = jsonPayload[key]
            current_user.set_attribute(key,value)

        current_user.save()
        return Response({"ok":"ok"})


