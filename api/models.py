from django.db import models
from django.contrib.auth.models import AbstractUser
from django.utils.translation import ugettext_lazy as _
from django.conf import settings
import uuid
import jwt
from _datetime import datetime, timedelta
import functools
from django.utils.html import format_html
from django.core.files.storage import FileSystemStorage



def rsetattr(obj, attr, val):
    pre, _, post = attr.rpartition('.')
    return setattr(rgetattr(obj, pre) if pre else obj, post, val)

# using wonder's beautiful simplification: https://stackoverflow.com/questions/31174295/getattr-and-setattr-on-nested-objects/31174427?noredirect=1#comment86638618_31174427

def rgetattr(obj, attr, *args):
    def _getattr(obj, attr):
        return getattr(obj, attr, *args)
    return functools.reduce(_getattr, [obj] + attr.split('.'))
        # ====== Games =========
class Game(models.Model):
    key = models.CharField(max_length=100, primary_key=True)
    name= models.CharField(max_length=255)
    description= models.CharField(max_length=255)
    image= models.ImageField(null=True,upload_to='supported_games/')
    def __str__(self):
        return self.name
class Item(models.Model):
    code = models.CharField(max_length=255,primary_key=True)
    buy = models.ManyToManyField(settings.AUTH_USER_MODEL, blank=True, related_name='buy')
    sell = models.ManyToManyField(settings.AUTH_USER_MODEL, blank=True, related_name='sell')
    game = models.ForeignKey(Game,on_delete=models.CASCADE, related_name='item_field')
    description = models.CharField(max_length=255)
    type_description = models.CharField(max_length=255)
    type = models.CharField(max_length=2048)
    def __str__(self):
        return self.code
        # ====== Games =========


        # ====== PROFILES =========
class FacebookProfile(models.Model):
    user = models.OneToOneField(settings.AUTH_USER_MODEL,null=True, on_delete=models.CASCADE, related_name='facebook_profile')
    token = models.CharField(max_length=2048)
    email = models.EmailField(_('email address'), unique=True)
    name= models.CharField(null=True,max_length=256)
    fb_id= models.CharField(null=True,max_length=256)
    picture = models.CharField(null=True,max_length=2048)
    def __str__(self):
        return "{}".format(self.email)
class GoogleProfile(models.Model):
    user = models.OneToOneField(settings.AUTH_USER_MODEL,null=True, on_delete=models.CASCADE, related_name='google_profile')
    token = models.CharField(max_length=2048)
    email = models.EmailField(_('email address'), unique=True)
    name= models.CharField(null=True,max_length=256)
    picture = models.CharField(null=True,max_length=2048)
class UserProfile(models.Model):
    user = models.OneToOneField(settings.AUTH_USER_MODEL,null=True, on_delete=models.CASCADE, related_name='profile')
    dob = models.DateField(null=True,blank=True)
    country = models.CharField(null=True,max_length=50,blank=True)
    city = models.CharField(null=True,max_length=50,blank=True)
    photo = models.ImageField(null=True,upload_to='users/', blank=True)
    full_name = models.CharField(null=True,max_length=50,blank=True)
    # ====== PROFILES =========







class User(AbstractUser):
    id = models.AutoField(primary_key=True)
    username = models.CharField(max_length=128, unique=True)
    email = models.EmailField(_('email address'), unique=True)
    USERNAME_FIELD = 'email'
    REQUIRED_FIELDS = ['username']

    def get_attribute(self,key):
        rgetattr(self,key)

    def set_attribute(self,key,value):
        rsetattr(self,key,value)
        self.save()
        self.profile.save()
        try:
            self.google_profile.save()
        except User.google_profile.RelatedObjectDoesNotExist:
            pass
        try:
            self.facebook_profile.save()
        except User.facebook_profile.RelatedObjectDoesNotExist:
            pass
    def __str__(self):
        return "{}".format(self.email)



