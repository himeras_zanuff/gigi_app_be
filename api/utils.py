from rest_framework.decorators import api_view
from rest_framework.response import Response
from django.http import request
from api.models import User, UserProfile,Item
import json
import jwt
@api_view(['POST'])
def upload_profile_pic(request):
    auth_header = request.META.get('HTTP_AUTHORIZATION')
    decoded_jwt = jwt.decode(auth_header.split(' ')[1],'n&dc^@zl(ky18fz4ib==4ilyxe%iwa7717#7)&*816!h$u*(3k')
    current_user = User.objects.get(id=decoded_jwt['user_id'])
    current_user.set_attribute('profile.photo',request.FILES.getlist('file')[0])
    return Response({"result":"Success"})
@api_view(['PUT'])
def update_sell_items(request):
    auth_header = request.META.get('HTTP_AUTHORIZATION')
    decoded_jwt = jwt.decode(auth_header.split(' ')[1],'n&dc^@zl(ky18fz4ib==4ilyxe%iwa7717#7)&*816!h$u*(3k')
    current_user = User.objects.get(id=decoded_jwt['user_id'])
    json_payload= json.loads(request.body.decode('utf-8'))
    for i in json_payload:
        key= i
        if json_payload[i]=='X':
            current_user.sell.add(Item.objects.get(code=key))
        else:
            current_user.sell.remove(Item.objects.get(code=key))
        print(i,json_payload[i])
    return Response({"result":"Success"})
@api_view(['PUT'])
def update_buy_items(request):
    auth_header = request.META.get('HTTP_AUTHORIZATION')
    decoded_jwt = jwt.decode(auth_header.split(' ')[1],'n&dc^@zl(ky18fz4ib==4ilyxe%iwa7717#7)&*816!h$u*(3k')
    current_user = User.objects.get(id=decoded_jwt['user_id'])
    json_payload= json.loads(request.body.decode('utf-8'))
    for i in json_payload:
        key= i
        if json_payload[i]=='X':
            current_user.buy.add(Item.objects.get(code=key))
        else:
            current_user.buy.remove(Item.objects.get(code=key))
        print(i,json_payload[i])
    return Response({"result":"Success"})


