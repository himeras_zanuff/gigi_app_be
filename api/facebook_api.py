from api.models import FacebookProfile,User
from django.http import request
from rest_framework_simplejwt.tokens import RefreshToken
from rest_framework.decorators import api_view
from rest_framework.decorators import authentication_classes, permission_classes
from rest_framework.response import Response
import requests
import json
import jwt
import uuid

def fb_get_create_user(fb_profile,fb_data):
    try:
        user = User.objects.get(facebook_profile=fb_profile)
    except User.DoesNotExist:
        try:
            user = User.objects.get(email=fb_data['email'])
            user.facebook_profile = fb_profile
            fb_profile.user = user
            fb_profile.save()
            user.save()
        except User.DoesNotExist:
            user = User.objects.create(email=fb_data['email'],username=uuid.uuid1(),facebook_profile=fb_profile)
            fb_profile.user = user
            fb_profile.save()
            user.save()
    return user


def fb_get_data(fb_token):
    URL = 'https://graph.facebook.com/v6.0/me'
    HEADERS= {"Accept": "application/json"}
    PARAMS= {'fields':'name,picture.width(800).height(800),email,id','access_token':fb_token}
    fbResponse = requests.get(url = URL, params = PARAMS,headers = HEADERS)
    return fbResponse.json()

def fb_get_create_profile(fb_token,fb_data):
    try:
        fb_profile = FacebookProfile.objects.get(email = fb_data['email'])
    except FacebookProfile.DoesNotExist:
        fb_profile = FacebookProfile.objects.create(email=fb_data['email'],token=fb_token)
    fb_profile.save()
    return fb_profile

def fb_update_profile(fb_profile,fb_data):
    fb_profile.name = fb_data['name']
    fb_profile.fb_id = fb_data['id']
    fb_profile.picture = fb_data['picture']['data']['url']
    fb_profile.save()

def get_tokens_for_user(user):
    refresh = RefreshToken.for_user(user)
    return {
        'refresh': str(refresh),
        'access': str(refresh.access_token),
    }

@api_view(['POST'])
@authentication_classes([])
@permission_classes([])
def fb_login(request):
    jsonPayload = json.loads(request.body.decode('utf-8'))
    fb_token = jsonPayload['token']
    fb_data = fb_get_data(fb_token)
    fb_profile = fb_get_create_profile(fb_token,fb_data)
    fb_update_profile(fb_profile,fb_data)
    user = fb_get_create_user(fb_profile,fb_data)
    user.profile.full_name = fb_profile.name
    user.save()
    jwt_token = get_tokens_for_user(user)
    return Response({"JWT": jwt_token})

@api_view(['POST'])
def fb_link(request):
    jsonPayload = json.loads(request.body.decode('utf-8'))
    fb_token = jsonPayload['token']
    auth_header = request.META.get('HTTP_AUTHORIZATION')
    decoded_jwt = jwt.decode(auth_header.split(' ')[1],'n&dc^@zl(ky18fz4ib==4ilyxe%iwa7717#7)&*816!h$u*(3k')
    user = User.objects.get(id=decoded_jwt['user_id'])
    fb_data = fb_get_data(fb_token)
    fb_profile = fb_get_create_profile(fb_token,fb_data)
    fb_update_profile(fb_profile,fb_data)
    fb_profile.user = user
    fb_profile.save()
    user.profile.full_name = fb_profile.name 
    user.facebook_profile = fb_profile
    user.save()
    jwt_token = get_tokens_for_user(user)
    return Response({"JWT": jwt_token})