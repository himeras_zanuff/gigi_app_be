from rest_framework import routers
from django.urls import path,include
from django.conf.urls import url
from django.conf import settings
from django.conf.urls.static import static
from django.contrib import admin
from chat.views import *

router = routers.DefaultRouter()
router.register(r'messages', MessageViewSet, basename='Message')
router.register(r'conversations', ConversationViewSet, basename='Conversation')

urlpatterns = [
    url(r'^', include(router.urls)),
    path('', index, name='index'),
    path('<str:room_name>/', room, name='room'),
]

