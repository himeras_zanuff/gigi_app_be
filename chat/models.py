from django.db import models
from api.models import User

class Conversation(models.Model):
    participant_1= models.ForeignKey(User,on_delete=models.CASCADE,related_name="conversations_1")
    participant_2= models.ForeignKey(User,on_delete=models.CASCADE,related_name="conversations_2")
    room_number = models.CharField(max_length=255)
    def __str__(self):
        return self.participant_1.email + ":"+self.participant_2.email

class Message(models.Model):
    # key = models.CharField(max_length=100, primary_key=True)
    conversation = models.ForeignKey(Conversation,max_length=255,on_delete=models.CASCADE,related_name="messages")
    sender= models.ForeignKey(User,max_length=255,on_delete=models.CASCADE)
    timestamp= models.DateTimeField(auto_now_add=True)
    message= models.CharField(blank=True,max_length=255) 
    def __str__(self):
        return self.sender.email + "__"+str(self.timestamp)+"__"+self.content
