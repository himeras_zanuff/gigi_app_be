# chat/consumers.py
import json
from asgiref.sync import async_to_sync, sync_to_async
from channels.generic.websocket import AsyncWebsocketConsumer
import jwt
from api.models import User
from django.contrib.auth.models import AnonymousUser
from channels.db import database_sync_to_async
from chat import models
class ChatConsumer(AsyncWebsocketConsumer):
    @database_sync_to_async
    def get_user(self,user_id):
        return User.objects.get(id=user_id)
    @database_sync_to_async
    def get_create_conversation(self,sending_user,recieving_user):
        room_number = str(sending_user.id)+"-"+str(recieving_user.id)
        try:
            conv = models.Conversation.objects.get(participant_1=sending_user,participant_2=recieving_user)
        except models.Conversation.DoesNotExist:
            try:
                conv = models.Conversation.objects.get(participant_2=sending_user,participant_1=recieving_user)
            except models.Conversation.DoesNotExist:
                conv = models.Conversation.objects.create(participant_1=sending_user,participant_2=recieving_user,room_number=room_number)
        conv.save()
        print('conv.room_number',conv.room_number)
        return conv

    async def connect(self):
        headers = dict(self.scope['headers'])
        try:
            print(1)
            token_name, token_key = headers[b'authorization'].decode().split()
            decoded_jwt = jwt.decode(token_key,'n&dc^@zl(ky18fz4ib==4ilyxe%iwa7717#7)&*816!h$u*(3k')
            sending_user = await self.get_user(decoded_jwt["user_id"])
            recieving_user = await self.get_user(self.scope['url_route']['kwargs']['reciever'])
            self.scope['sending_user'] = sending_user
            self.scope['recieving_user'] = recieving_user
        except User.DoesNotExist:
            print("User not found")
            scope['sending_user'] = AnonymousUser()
        except:
            sending_user = await self.get_user(30)
            recieving_user = await self.get_user(self.scope['url_route']['kwargs']['reciever'])
            self.scope['sending_user'] = sending_user
            self.scope['recieving_user'] = recieving_user
            print("some error occured")
            # await self.close()
            # return

        self.conversation = await self.get_create_conversation(sending_user,recieving_user)
        self.room_name = self.conversation.room_number
        print("ROOM NAME: ",self.room_name)
        self.room_group_name = 'chat_%s' % self.room_name

        # Join room group
        # print(self.channel_name)
        await self.channel_layer.group_add(
            self.room_group_name,
            self.channel_name
        )
        # print(self.scope['sending_user'])
        await self.accept()

    async def disconnect(self, close_code):
        # Leave room group
        try:
            await self.channel_layer.group_discard(
                self.room_group_name,
                self.channel_name
            )
        except:
            pass

    @database_sync_to_async
    def store_message(self,message):
        new_message = models.Message(conversation=self.conversation,
                        sender=self.scope['sending_user'],
                        message=message)
        new_message.save()

    # Receive message from WebSocket
    async def receive(self, text_data):
        print(text_data)
        text_data_json = json.loads(text_data)
        message = text_data_json['message']
        sender = text_data_json['sender']
        await self.store_message(message)
        # Send message to room group
        await self.channel_layer.group_send(
            self.room_group_name,
            {
                'type': 'chat_message',
                'message': message,
                'sender':sender
            }
        )

    # Receive message from room group
    async def chat_message(self, event):
        message = event['message']
        sender = event['sender']
        print(event)
        # Send message to WebSocket
        await self.send(text_data=json.dumps({
            'message': message,
            'sender':sender
        }))