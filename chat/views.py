from django.shortcuts import render
from chat.models import Message,Conversation
from djoser import permissions
from chat.serializers import MessageSerializer,ConversationSerializer
from rest_framework import viewsets
from django.db.models import Q
from django_filters import rest_framework as filters
def index(request):
    return render(request, 'chat/index.html')

def room(request, room_name):
    return render(request, 'chat/room.html', {
        'room_name': room_name
    })

class MessageViewSet(viewsets.ModelViewSet):
    permission_classes=[permissions.CurrentUserOrAdminOrReadOnly,]
    serializer_class = MessageSerializer
    filter_backends = (filters.DjangoFilterBackend, )
    filter_fields = ('conversation',)
    def get_queryset(self):
        return Message.objects.filter(Q(conversation__participant_1=self.request.user.id) | Q(conversation__participant_2=self.request.user.id))

class ConversationViewSet(viewsets.ModelViewSet):
    permission_classes=[permissions.CurrentUserOrAdminOrReadOnly,]
    serializer_class = ConversationSerializer
    def get_queryset(self):
        return Conversation.objects.filter(Q(participant_1=self.request.user.id) | Q(participant_2=self.request.user.id))