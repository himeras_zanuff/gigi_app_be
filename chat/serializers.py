from chat.models import Message, Conversation
from rest_framework import serializers
from api.serializers import UserSerializer


class MessageSerializer(serializers.ModelSerializer):
    class Meta:
        model = Message
        fields = (
            'sender',
            'timestamp',
            'message',
        )


class ConversationSerializer(serializers.ModelSerializer):
    last_message = serializers.SerializerMethodField('get_last_message')
    partner = serializers.SerializerMethodField('get_partner')
    def get_partner(self, conversation):
        if (conversation.participant_1 == self.context['request'].user):
            partner = conversation.participant_2
        else:
            partner = conversation.participant_1

        picture = partner.profile.photo or 'http://10.0.2.2:8000/media/placeholder.png'
        name = partner.profile.full_name or "No full name"
        return {
            "picture": picture,
            "name": name,
            "id":partner.id
        }

    def get_last_message(self, conversation):
        try:
            qs = conversation.messages.order_by('-timestamp')[0]
            serializer = MessageSerializer(instance=qs, many=False)
        except:
            return None
        return serializer.data

    class Meta:
        model = Conversation
        fields = (
            'last_message',
            'partner',
            'id'
        )
